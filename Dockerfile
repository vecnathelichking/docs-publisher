# Base production image for CiviCRM Docs Publisher
FROM php:8.2-fpm-alpine as docs-publisher
ARG TIMEZONE
## Allow use of development versions of Symfony
ARG STABILITY="stable"
ENV STABILITY ${STABILITY}
## Allow selection of Symfony version
ARG SYMFONY_VERSION=""
ENV SYMFONY_VERSION ${SYMFONY_VERSION}
## Set application environment type
ENV APP_ENV=prod
## Set working directory
WORKDIR /srv/docs-publisher
## PHP extensions installer: https://github.com/mlocati/docker-php-extension-installer
COPY --from=mlocati/php-extension-installer --link /usr/bin/install-php-extensions /usr/local/bin/
# Setup / install runtime dependencies
RUN apk add --no-cache \
	acl \
	fcgi \
	file \
	gettext \
	git \
	unzip \
	py3-pip \
	;
RUN set -eux; \
	install-php-extensions \
	intl \
	zip \
	apcu \
	opcache \
	;
RUN pip install mkdocs-material==8.5.11
## Copy in scaffolding and preference files
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
COPY --link docker/php/conf.d/app.ini $PHP_INI_DIR/conf.d/
COPY --link docker/php/conf.d/app.prod.ini $PHP_INI_DIR/conf.d/
COPY --link docker/php/php-fpm.d/zz-docker.conf /usr/local/etc/php-fpm.d/zz-docker.conf
RUN mkdir -p /var/run/php
## Setup healthcheck
COPY --link docker/php/docker-healthcheck.sh /usr/local/bin/docker-healthcheck
RUN chmod +x /usr/local/bin/docker-healthcheck
## Schedule healthcheck
HEALTHCHECK --interval=10s --timeout=3s --retries=3 CMD ["docker-healthcheck"]
## Setup entrypoint
COPY --link docker/php/docker-entrypoint.sh /usr/local/bin/docker-entrypoint
RUN chmod +x /usr/local/bin/docker-entrypoint
ENTRYPOINT ["docker-entrypoint"]
CMD ["php-fpm"]
# Composer Setup
## https://getcomposer.org/doc/03-cli.md#composer-allow-superuser
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV PATH="${PATH}:/root/.composer/vendor/bin"
## Copy in Composer
COPY --from=composer/composer:2-bin --link /composer /usr/bin/composer
## Don't reinstall entire vendor tree on a code change
COPY composer.* symfony.* ./
RUN set -eux; \
	if [ -f composer.json ]; then \
	composer install --prefer-dist --no-dev --no-autoloader --no-scripts --no-progress; \
	composer clear-cache; \
	fi
# Application Setup
## Copy sources
COPY --link  . .
## Cleanup docker folder
RUN rm -Rf docker/
## Set file / directory permissions
RUN set -eux; \
	mkdir -p var/cache var/log; \
	if [ -f composer.json ]; then \
	composer dump-autoload --classmap-authoritative --no-dev; \
	composer dump-env prod; \
	composer run-script --no-dev post-install-cmd; \
	chmod +x bin/console; sync; \
	bin/console docs:publish; \
	fi
## Set timezone
RUN ln -snf /usr/share/zoneinfo/${TIMEZONE} /etc/localtime && echo ${TIMEZONE} > /etc/timezone \
	&& printf '[PHP]\ndate.timezone = "%s"\n', ${TIMEZONE} > /usr/local/etc/php/conf.d/tzone.ini \
	&& "date"
# Development image for CiviCRM Docs Publisher
FROM docs-publisher AS docs-publisher-dev
## Set application environment type
ENV APP_ENV=dev XDEBUG_MODE=off
## Set working directory
VOLUME /srv/docs-publisher/var/
## Copy in scaffolding and preference files
RUN rm $PHP_INI_DIR/conf.d/app.prod.ini; \
	mv "$PHP_INI_DIR/php.ini" "$PHP_INI_DIR/php.ini-production"; \
	mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"
COPY --link docker/php/conf.d/app.dev.ini $PHP_INI_DIR/conf.d/
## Install development dependencies
RUN set -eux; \
	install-php-extensions xdebug
## Remove local environment variable file.
RUN rm -f .env.local.php
# Web Server image for CiviCRM Docs Publisher
## Build Caddy with the Mercure and Vulcain modules
FROM caddy:2.6-builder-alpine AS docs-publisher-caddy-builder
RUN xcaddy build \
	--with github.com/dunglas/mercure \
	--with github.com/dunglas/mercure/caddy \
	--with github.com/dunglas/vulcain \
	--with github.com/dunglas/vulcain/caddy
## Caddy hosting image.
FROM caddy:2.6-alpine AS docs-publisher-caddy

WORKDIR /srv/docs-publisher

COPY --from=docs-publisher-caddy-builder --link /usr/bin/caddy /usr/bin/caddy
COPY --from=docs-publisher --link /srv/docs-publisher/public public/
