var input = document.querySelectorAll('input');
for(i=0; i<input.length; i++){
    input[i].setAttribute('size',input[i].getAttribute('placeholder').length);
}
document.querySelector('.extensions .book-cards').classList.add('js-filter');
let body = document.getElementsByTagName('body');
let themeSwitches = document.getElementsByClassName('dp-theme-button');
for (let i of themeSwitches) {
    i.addEventListener('click', function () {
        let theme = this.dataset.dpTheme;
        body[0].dataset.dpTheme = theme;
    })
}
document.addEventListener('DOMContentLoaded', function() {
    let body = document.getElementsByTagName('body');
    let theme
    if (window.matchMedia('(prefers-color-scheme: light)').matches) {
        theme = 'civicrm-light'
    };
    if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
        theme = 'civicrm-dark'
    };
    body[0].dataset.dpTheme = theme;
})