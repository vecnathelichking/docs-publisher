document.onreadystatechange = () => {
	var input, filter, ul, li, tags;
	input = document.getElementById('search-input');
	input.onkeyup = function (){
		filter = input.value.toLowerCase();
		ul = document.querySelector('.book-cards.js-filter');
		li = ul.getElementsByTagName('li');

		console.log(filter);

		for (i = 0; i < li.length; i++) {
			console.log('Current Item: ' + i)
			tags = li[i].getAttribute('data-tags');
			if (tags) {
				if (tags.search(filter) > -1) {
					li[i].style.display = "";
				} else {
					li[i].style.display = "none";
				}
			}
		}
	}
}