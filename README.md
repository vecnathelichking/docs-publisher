# CiviCRM documentation infrastructure

[![pipeline status](https://lab.civicrm.org/documentation/docs-publisher/badges/master/pipeline.svg)](https://lab.civicrm.org/documentation/docs-publisher/-/commits/master) [![coverage report](https://lab.civicrm.org/documentation/docs-publisher/badges/master/coverage.svg)](https://lab.civicrm.org/documentation/docs-publisher/-/commits/master)

This repository holds source code for the *infrastructure* CiviCRM uses to host and update various documentation books built with [MkDocs](http://mkdocs.org/) and published to [docs.civicrm.org](https://docs.civicrm.org).

You may also wish to:

- [*Read* these documentation books](https://civicrm.org/documentation) *(and other sources of documentation)*
- [*Contribute* to documentation content](https://docs.civicrm.org/dev/en/master/documentation/)
- [*Add* a new documentation book](https://lab.civicrm.org/documentation/docs-books)

## Defining new books

This information/content has moved to a new repository. Click here to see the [docs-books repository](https://lab.civicrm.org/documentation/docs-books).

## Publishing updates to a book

Books are automatically published when the corresponding branch is updated their repository. This is typically achieved by making edits and submitting a pull request. Any emails listed in the commits that are submitted as part of the pull request will receive an email with a summary of the update process.

### Setting up automatic publishing

#### GitHub

Auto updates are configured via webhooks within the repository on GitHub. You will need to be an owner (not just a collaborator) of the repository in order to perform these steps.

1. Go to `https://github.com/[user-or-group-name]/[repo-name]/settings/hooks/new`
2. Set the **Payload URL** to `https://docs.civicrm.org/admin/listen`
3. Set the **Content type** to 'application/json'
4. Set **Which events would you like to trigger this webhook?** to 'Let me select individual events' and select 'Pull request' and 'Push' (since these are the only events that should trigger an update)

#### GitLab

1. Go to `https://lab.civicrm.org/[user-or-group-name]/[repo-name]/settings/integrations`
2. Set the **URL** to `https://docs.civicrm.org/admin/listen`
3. Set the **Trigger** to 'Push events' and 'Tag push events'.

### Manual publishing

If required, a book can be manually updated by calling a URL in the following format.

```text
https://docs.civicrm.org/admin/publish/{book}/{lang}/{branch}
```

- `{book}` the name of the book - as per configuration file in the conf/books directory.
- `{lang}` the language that you want to publish - as defined in the configuration file **(Optional)**.
- `{branch}` the name of the branch that you want to publish - needs to be a branch in the repository for that language **(Optional)**.

## Installing a local copy of the docs infrastructure

### Docker

The repo includes a dockerfile which you can use to create a container which has everything needed to run the application.

To build the container and install composer dependencies just run (from the project directory):

```bash
docker build -t docs-publisher .
docker run --rm -v $PWD:/var/www docs-publisher composer install --working-dir=/var/www
```

And then to run it:

```bash
docker run --rm -v $PWD:/var/www -p 8080:8080 docs-publisher
```

You might want to change the first 8080 in the port argument if you've already got something listening on that port.

The `nginx` user in the container will need to be able to write to these directories.

```bash
sudo chmod -R a+rw var/cache var/logs/ web/dev/ var/repos/
```

You should be able to see the app at `http://localhost:8080`.

### On your host machine

**Note**: the following steps are only useful and necessary for people looking after CiviCRM's documentation *infrastructure*. You don't need to do this if you just want to [contribute to documentation content](https://docs.civicrm.org/dev/en/master/documentation/).

1. Install the pre-requisites:
  **Note**: the example commands have been tested on Ubuntu 16.04 and 18.04.
   - [nginx](https://nginx.org):

     ``` bash
     sudo apt install nginx
     ```

   - [curl](https://curl.haxx.se/):

     ``` bash
     sudo apt install curl
     ```

   - [unzip](http://www.info-zip.org/UnZip.html):

     ``` bash
     sudo apt install unzip
     ```

   - [php 7.4](https://php.net):

     ``` bash
     sudo apt install software-properties-common
     sudo add-apt-repository ppa:ondrej/php
     sudo apt update
     sudo apt install php7.4-fpm php7.4-cli php7.4-gd php7.4-imagick php7.4-json php7.4-mbstring php7.4-opcache php7.4-readline php7.4-tidy php7.4-xml php7.4-xmlrpc php7.4-zip
     ```

   - [pip](https://packaging.python.org/en/latest/install_requirements_linux/#installing-pip-setuptools-wheel-with-linux-package-managers):

     ``` bash
     sudo apt install python3-pip
     ```

   - [composer](https://getcomposer.org)

     ``` bash
     php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
     php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
     php composer-setup.php
     php -r "unlink('composer-setup.php');"
     sudo mv composer.phar /usr/local/bin/composer
     ```

   - [mkdocs-material](https://mkdocs.org)

     ``` bash
     sudo -H pip install mkdocs-material==7.3.0
     ```

     **Note**: Ensure that MkDocs is installed as root so that it can be accessed from the src/publish.php script (typically invoked as `https://docs.civicrm.org/publish.php`)

2. clone this repository to somewhere like /var/www/civicrm-docs and install with composer:

    ```bash
    git clone https://lab.civicrm.org/documentation/docs-publisher.git /var/www/civicrm-docs
    cd /var/www/civicrm-docs
    composer install
    ```

3. Set appropriate permissions on web/static.

4. Using the example configuration file from `examples/nginx` create an nginx configuration file for the docs-publisher site. You'll need to check/adjust the PHP Socket path and the root path.

5. Configure the nginx virtual host:

    ``` bash
    ln -s /etc/nginx/sites-available/civicrm-docs /etc/nginx/sites-enabled/
    ```

6. Grab the documentation book files:

    ``` bash
    cd /var/www/civicrm-docs/
    git clone https://lab.civicrm.org/documentation/docs-books.git books

7. Reload your nginx config and you should be up and running.

### Debugging

You will need xdebug installed and configured to debug from your IDE. In this case
we assume you're using PHPStorm.

The docker image comes with xdebug pre-installed and configured. From there the steps you need to take to get it working are:

- From "Settings > Languages and Frameworks > PHP > Debug" change the xdebug port to 9000
- From "Settings > Languages and Frameworks > PHP > Servers" add a new server with any name, host of "localhost", port of 8080.
- In the same screen enable path mappings and map the project directory to "/var/www" under "Absolute path on the server"
- Install the xdebug helper for [chrome](https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc?hl=en) or [firefox](https://addons.mozilla.org/en-gb/firefox/addon/the-easiest-xdebug/), setting the IDE key to "PHPSTORM" if necessary.
- Enable the xdebug helper, put a breakpoint somewhere in web/app_dev.php and visit `http://localhost:8080` and the debugger should open PHPStorm
