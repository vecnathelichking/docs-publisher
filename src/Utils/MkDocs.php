<?php

namespace App\Utils;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Process\Process;
use Psr\Log\LoggerInterface;

class MkDocs {

    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * @var FileLocator
     */
    private $fileLocator;

    /**
     * @var string
     * The full filesystem path to the directory containing the markdown files
     */
    private $sourcePath;

    /**
     * @var string $destinationPath
     * The full filesystem path to the directory where we want the published
     * content to go
     */
    private $destinationPath;

    /**
     * @var string
     * The full filesystem path to the directory which stores different
     * possible theme customizations. Within this directory, separate
     * directories should exist, per theme, for the customizations, named with
     * the same name as the theme.
     */
    private $themeCustomPathRoot;

    /**
     * @var string The full filesystem path to the directory
     */
    private $themeCustomPath;

    /**
     * @var array
     * A associative array with config values to put in the 'extra' setting
     * when building the book
     */
    private $extraConfig;

    /**
     * @var string
     * The full filesystem location of the mkdocs.yml config file to use when
     * building the book. This is the file as it's stored after adjustments we
     * make to it.
     */
    private $configFile;

    /**
     * @param Filesystem $fs
     *
     * @param FileLocator $fileLocator
     */
    public function __construct(Filesystem $fs, string $kernelRoot) {
        $this->fs = $fs;
        $themeOverrides = $kernelRoot . '/src/MkDocs/overrides';
        $this->themeCustomPathRoot = $themeOverrides;
    }

    /**
     * Reads the mkdocs.yml config file from the book source. Makes some
     * customizations to it, and the write the file into the directory where
     * we're going to publish the book. Why put it there? It doesn't need to be in
     * the publish destination, but it seems like as good a place as any. It just
     * needs to be stored somewhere so that mkdocs can read it while building the
     * book.
     */
    private function customizeConfig(): void {
        // Read config in
        $inFile = "{$this->sourcePath}/mkdocs.yml";
        $config = Yaml::parseFile($inFile);

        // Set the docs_dir for builds.
        $config['docs_dir'] = "{$this->sourcePath}/docs";

        // If we have a theme-customization directory which matches the theme used
        // in the book, then use these theme customizations when building.
        $theme = $config['theme']['name'];
        $this->themeCustomPath = "{$this->themeCustomPathRoot}/$theme";
        if ($this->fs->exists($this->themeCustomPath)) {
            $config['theme']['custom_dir'] = $this->themeCustomPath;
        }

        // Set extra config which was passed into build()
        foreach ($this->extraConfig as $key => $val) {
            $config['extra'][$key] = $val;
        }

        // Set up custom config for our Material theme extension
        if ($theme == 'material') {
            $config['theme']['logo'] = '/static/images/CiviCRMLogo.svg';
            $palette = [
                [
                    'media' => '(prefers-color-scheme: light)',
                    'scheme' => 'civicrm-light',
                    'toggle' => [
                        'icon' => 'material/lightbulb-outline',
                        'name' => 'Switch to dark mode'
                    ],
                    'primary' => 'indigo',
                    'accent' => 'green'
                ],
                [
                    'media' => '(prefers-colour-scheme: dark)',
                    'scheme' => 'civicrm-dark',
                    'toggle' => [
                        'icon' => 'material/lightbulb',
                        'name' => 'Switch to light mode'
                    ],
                    'primary' => 'indigo',
                    'accent' => 'green'
                ]
            ];
            $config['theme']['palette'] = $palette;
            $config['theme']['include_search_page'] = FALSE;
            $config['theme']['features'] = 'navigation.sections';
            $config['theme']['features'] = 'navigation.top';
            $config['theme']['features'] = 'toc.integrate';
            // Turning off the "Instant" loading feature - breaks some of our custom symfony links at the moment.
            // $config['theme']['features'][] = 'instant';
            $plausibledomain = $_ENV['PLAUSIBLE_DOMAIN'];
            $config['extra']['analytics']['provider'] = 'custom';
            $config['extra']['analytics']['domain'] = $plausibledomain;
            $config['extra']['consent']['title'] = 'Cookie Consent';
            $config['extra']['consent']['description'] = "We use cookies to recognize your repeated visits and preferences, as well as to measure the effectiveness of our documentation and whether users find what they're searching for. With your consent, you're helping us to make our documentation better.";
            $config['extra_css'][] = 'assets/stylesheets/extra.css';
            $config['extra']['year'] = date("Y");
            if (!isset($config['extra']['edition'])) {
                $config['extra']['edition'] = "English / Latest";
            }
            if (!isset($config['extra']['book_home'])) {
                $config['extra']['book_home'] = "/";
            }
            $config['markdown_extensions'][]['pymdownx.superfences']['css_class'] = 'highlight';
            $config['markdown_extensions'][]['pymdownx.inlinehilite']['css_class'] = 'highlight';
        }

        // Dump config out
        $yaml = Yaml::dump($config);
        $this->configFile = dirname($this->destinationPath) . "/"
        . basename($this->destinationPath) . "-mkdocs.yml";
        $this->fs->dumpFile($this->configFile, $yaml);
    }

    private function getOptions(): string {
        // discard existing build files -- build site from scratch
        $opts[] = "--clean";

        // abort the build if any errors occur
        $opts[] = "--strict";

        // use our customized config file
        $opts[] = "--config-file {$this->configFile}";

        // this is where the finished site should go
        $opts[] = "--site-dir {$this->destinationPath}";

        return implode(" ", $opts);
    }

    /**
     * Run MkDocs to build a book
     *
     * @param string $sourcePath
     * The full filesystem path to the directory containing the markdown files
     *
     * @param string $destinationPath
     * The full filesystem path to the directory where we want the published
     * content to go
     *
     * @param array $extraConfig
     * A associative array with config values to put in the 'extra' setting
     * when building the book
     *
     * @throws \Exception
     * If build process fails
     */
    public function build($sourcePath, $destinationPath, $extraConfig = array()): void {
        $this->sourcePath = $sourcePath;
        $this->destinationPath = $destinationPath;
        $this->extraConfig = $extraConfig;

        $this->customizeConfig();

        $buildCommand = "mkdocs build " . $this->getOptions();
        $mkdocs = Process::fromShellCommandline($buildCommand, $this->sourcePath);
        $mkdocs->run();
        if (!$mkdocs->isSuccessful()) {
            throw new \Exception("MkDocs was unable to build the book. "
            . "MkDocs command output: "
            . $mkdocs->getErrorOutput());
        }
    }
}
