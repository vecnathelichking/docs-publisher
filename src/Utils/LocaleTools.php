<?php

namespace App\Utils;

use Symfony\Component\Intl\Locales;

/**
 * A set of helper tools for dealing with different locales
 *
 */
class LocaleTools {
        /**
     * Returns the name of a language, in another language. For example, if
     * $languageCode = 'en' and $localeCode = 'es' this function would answer the
     * question "what word do Spanish-speaking people use to refer to English?"
     *
     * @param string $languageCode
     * The language we're asking about
     *
     * @param string $localeCode
     * The language in which we want our answer
     *
     * @return string
     */
    public static function getLanguageNameInLocale($languageCode, $localeCode): string {
        $localeName = Locales::getName($languageCode, $localeCode);
        return $localeName;
    }

    /**
     * Checks to see whether a given language code is a valid ISO-639-1 code.
     *
     * @param string $languageCode
     * e.g. "en", or "es"
     *
     * @return boolean
     * TRUE if the code is valid
     */
    public static function codeIsValid($languageCode): bool {
        $isValidLocale = Locales::exists($languageCode);
        return $isValidLocale;
    }
}
