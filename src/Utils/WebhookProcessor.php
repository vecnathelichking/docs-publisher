<?php

namespace App\Utils;

use App\Model\WebhookEvent;
use App\Utils\WebhookAdapters\WebhookHandler;
use Symfony\Component\HttpFoundation\Request;

class WebhookProcessor {
    /**
     * @var WebhookHandler[]
     */
    protected $handlers = [];

    /**
     * @param WebhookHandler[] $handlers
     */
    public function __construct(array $handlers) {
        $this->handlers = $handlers;
    }

    /**
     * Process a webhook
     *
     * @param Request $request
     *
     * @return WebhookEvent
     *
     * @throws \Exception
     */
    public function process(Request $request): WebhookEvent {
        $event = null;
        
        foreach ($this->handlers as $handler) {
            if ($handler->canHandle($request)) {
                $event = $handler->handle($request);
                break;
            }
        }

        if (!$event) {
            throw new \Exception('Could not handle webhook event');
        }

        if ($event->getType() != 'push') {
            throw new \Exception("Webhook event type is not 'push'");
        }

        return $event;
    }
}
