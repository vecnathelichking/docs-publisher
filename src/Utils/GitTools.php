<?php

namespace App\Utils;

use Symfony\Component\Process\Process;

class GitTools {
    /**
     * @param $repoPath
     *
     * @return string
     */
    public function getCurrentBranch($repoPath): string {
        $process = new Process(['git', 'rev-parse', '--abbrev-ref', 'HEAD']);
        $process->setWorkingDirectory($repoPath);
        $process->run();
        $currentBranch = trim($process->getOutput());

        return $currentBranch;
    }

    /**
     * @param string $repoPath
     */
    public function pull($repoPath): void {
        $process = new Process(['git', 'pull']);
        $process->setWorkingDirectory($repoPath);
        $process->run();

        if (!$process->isSuccessful()) {
            $msg = "Unable to run 'git pull'. Output: " . $process->getErrorOutput();
            throw new \Exception($msg);
        }
    }

    /**
     * @param $repoURL
     * @param $repoPath
     *
     * @throws \Exception
     */
    public function clone($repoURL, $repoPath): void {
        $process = new Process(['git', 'clone', $repoURL, $repoPath]);
        $process->run();

        if (!$process->isSuccessful()) {
            $msg = "Unable to run 'git clone'. Output: " . $process->getErrorOutput();
            throw new \Exception($msg);
        }
    }

    /**
     * @param $repoPath
     *
     * @throws \Exception
     */
    public function fetch($repoPath): void {
        $process = new Process(['git', 'fetch']);
        $process->setWorkingDirectory($repoPath);
        $process->run();

        if (!$process->isSuccessful()) {
            $msg = "Unable to run 'git fetch'. Output: " . $process->getErrorOutput();
            throw new \Exception($msg);
        }
    }

    /**
     * If we are on the not on the correct branch, attempt to check it out
     * (first locally, then remotely).
     *
     * @param string $repoPath
     * @param string $branch
     */
    public function checkout(string $repoPath, string $branch): void {
        $currentBranch = $this->getCurrentBranch($repoPath);

        if ($currentBranch != $branch) {
            if (!$this->localBranchExists($repoPath, $branch)) {
                if (!$this->remoteBranchExists($repoPath, $branch)) {
                    $err = "'{$branch}' branch does not exist " . "remotely or locally.";
                    throw new \Exception($err);
                }
            }

            $process = new Process(['git', 'checkout', $branch]);
            $process->setWorkingDirectory($repoPath);
            $process->run();

            if (!$process->isSuccessful()) {
                $err = "Unable to run 'git checkout'\n" . $process->getErrorOutput();
                throw new \Exception($err);
            }
        }
    }

    /**
     * Get the most recent commit in our git directory.
     * 
     * @param string $repoPath
     */
    public function lastCommit(string $repoPath): string {
        $process = new Process(['git', 'rev-list', '--format=%B', '--max-count=1', 'HEAD']);
        $process->setWorkingDirectory($repoPath);
        $process->run();

        if (!$process->isSuccessful()) {
            $msg = "Unable to get last commit with 'git rev-list'. Output: " . $process->getErrorOutput();
            throw new \Exception($msg);
        }

        $lastCommit = trim($process->getOutput());
        return $lastCommit;
    }

    /**
     * @param $repoPath
     * @param $branch
     *
     * @return bool
     */
    private function localBranchExists($repoPath, $branch): bool {
        $ref = "refs/heads/$branch";
        $process = new Process(['git', 'show-ref', '--verify', $ref]);
        $process->setWorkingDirectory($repoPath);
        $process->run();

        return $process->isSuccessful();
    }

    /**
     * @param $repoPath
     * @param $branch
     *
     * @return bool
     */
    private function remoteBranchExists($repoPath, $branch): bool {
        $ref = "refs/remotes/origin/$branch";
        $process = new Process(['git', 'show-ref', '--verify', $ref]);
        $process->setWorkingDirectory($repoPath);
        $process->run();

        return $process->isSuccessful();
    }
}
