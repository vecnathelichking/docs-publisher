<?php

namespace App\Controller;

use App\Model\WebhookEvent;
use App\Utils\WebhookProcessor;
use App\Utils\Publisher;
use App\Model\Library;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;

class PublishController extends AbstractController {
    /**
     * @var Publisher
     */
    private $publisher;

    /**
     * @var bool TRUE if the book was published without any errors
     */
    private $publishSuccess;

    public function __construct(
        Library $library,
        Publisher $publisher,
        WebhookProcessor $webhookprocessor,
        MailerInterface $mailer
    ) {
        $this->library = $library;
        $this->publisher = $publisher;
        $this->webhookprocessor = $webhookprocessor;
        $this->mailer = $mailer;
    }

    /**
     * @param string $identifier
     *
     * @return Response
     *
     * @Route("/admin/publish{identifier}" , requirements={"identifier": ".*"})
     */
    public function publishAction($identifier): Response {
        $bookSlug = Library::parseIdentifier($identifier)['bookSlug'];
        if ($bookSlug) {
            $this->publishSuccess = $this->publisher->publish($identifier);
        } else {
            $this->publisher->addMessage('INFO', "Publish action called without a book "
            . "specified, thus attempting to publish all books.");
            $this->publisher->addMessage('CRITICAL', "Publishing all books is not "
            . "supported through the web interface because it has the potential "
            . "to really slow down the server. If you want to publish all books "
            . "you can run 'docs:publish' from the command line interface.");
        }
            $content['identifier'] = trim($identifier, "/");
            $content['messages'] = $this->publisher->getMessages();
            return $this->render('publisher/publish.html.twig', $content);
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/admin/listen")
     */
    public function listenAction(Request $request): Response {
        $processor = $this->webhookprocessor;
        $event = $processor->process($request);

        $identifiers = $this->library->getIdentifiersByRepo($event->getRepo());

        if (!$identifiers) {
            $msg = "CRITICAL - No books found which match " . $event->getRepo();

            return new Response($msg, Response::HTTP_BAD_REQUEST);
        }

        foreach ($identifiers as $identifier) {
            $fullIdentifier = sprintf('%s/%s', $identifier, $event->getBranch());
            $this->publisher->publish($fullIdentifier);
            $this->sendEmail($fullIdentifier, $event);
        }
        $response = $this->publisher->getMessagesInPlainText();

        return new Response($response);
    }

    /**
     * Send notification emails after publishing
     *
     * @param string $identifier
     * @param WebhookEvent $event
     */
    private function sendEmail(string $identifier, WebhookEvent $event): void {
        /**
         * Array of strings for email addresses that should receive the
         * notification email. If none are specified, then the email will be sent to
         * all addresses set in the book's yaml configuration
         */
        $extraRecipients = $event->getNotificationRecipients();
        $commits = $event->getCommits();

        $messages = $this->publisher->getMessages();
        $parts = $this->library::parseIdentifier($identifier);

        /** @var \App\Model\Book */
        $book = $this->library->getBookBySlug($parts['bookSlug']);

        $language = $book->getLanguageByCode($parts['languageCode']);
        $version = $language->getVersionByDescriptor($parts['versionDescriptor']);
        $webPath = $version->url;

        $subject = "Publishing Successful";
        $rawRecipients = array_unique(array_merge($extraRecipients, $language->watchers));

        $recipients = [];
        foreach ($rawRecipients as $recipient) {
            $recipients[] = new Address($recipient);
        }

        $renderParams = [
            'publishURLBase' => $webPath,
            'status' => $subject,
            'messages' => $messages,
            'commits' => $commits,
        ];
        $body = $this->renderView('emails/notify.html.twig', $renderParams);
        $mail = (new Email())
            ->to(...$recipients)
            ->subject($subject)
            ->html($body);
        $this->mailer->send($mail);
    }
}
