<?php

namespace App\Model;

class WebhookEvent {
    const SOURCE_GITHUB = 'Github';
    const SOURCE_GITLAB = 'Gitlab';

    /**
     * @var string
     * Where did the hook original, e.g. github
     */
    protected $source = '';

    /**
     * @var string
     * What sort of event is it, for example "push"
     */
    protected $type = '';

    /**
     * @var string
     * The repo this event was fired from
     */
    protected $repo = '';

    /**
     * @var string
     * The branch the event was fired from
     */
    protected $branch = '';

    /**
     * @var \stdClass[]
     * An array of commits in the event, each commit will have properties "ID",
     * "author" and "message"
     */
    protected $commits = [];

    /**
     * @return string
     */
    public function getSource(): string {
        return $this->source;
    }

    /**
     * @param string $source
     * @return $this
     */
    public function setSource(string $source): static {
        $this->source = $source;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type): static {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getRepo(): string {
        return $this->repo;
    }

    /**
     * @param string $repo
     * @return $this
     */
    public function setRepo(string $repo): static {
        $this->repo = $repo;

        return $this;
    }

    /**
     * @return string
     */
    public function getBranch(): string {
        return $this->branch;
    }

    /**
     * @param string $branch
     * @return $this
     */
    public function setBranch(string $branch): static {
        $this->branch = $branch;

        return $this;
    }

    /**
     * @param array $commit
     */
    public function addCommit(array $commit): void {
        $this->commits[] = $commit;
    }

    /**
     * @param \stdClass[] $commits
     */
    public function setCommits(array $commits): void {
        $this->commits = $commits;
    }

    /**
     * @return \stdClass[]
     */
    public function getCommits() : array {
        return $this->commits;
    }

    /**
     * @return array
     */
    public function getCommitMessages() : array {
        return array_column($this->getCommits(), 'message');
    }

    /**
     * @return array
     * Email address of all those who should be notified about the event
     */
    public function getNotificationRecipients(): array {
        $recipients = [];

        foreach ($this->getCommits() as $commit) {
            $author = $commit->author ?? null;
            if (property_exists($author, 'email')) {
                $recipients[] = $author->email;
            }
        }

        // filter out mails that start with "donotreply" or "noreply"
        $recipients = preg_grep('/^(donot|no)reply@/', $recipients, PREG_GREP_INVERT);

        return array_unique($recipients);
    }
}
