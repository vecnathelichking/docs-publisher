<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\DependencyInjection\Dumper\Preloader;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use App\Utils\Paths;
use App\Model\Library;

class RepoClearCommand extends Command {
    protected static $defaultName = 'repo:clear';

    private $filesystem;

    public function __construct(Filesystem $filesystem = null, Paths $paths, Library $library)
    {
        parent::__construct();

        $this->filesystem = $filesystem ?: new Filesystem();
        $this->paths = $paths;
        $this->library = $library;
    }

    protected function configure() {
        $this
            ->setDescription('Clears one/all cached repositories from var/repos/')
            ->addArgument(
                'identifiers',
                InputArgument::IS_ARRAY | InputArgument::OPTIONAL,
                'One or more book names (e.g. "user" or "user/en"). '
                . 'Only "slug" or "slug/lang" is accepted.',
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $identifiers = $input->getArgument('identifiers');
        $fs = $this->filesystem;
        $io = new SymfonyStyle($input, $output);
        $kernel = $this->getApplication()->getKernel();
        $repoRoot = $this->paths->getRepoPathRoot();
        if ($identifiers) {
            foreach ($identifiers as $identifier) {
                $parts = $this->library::parseIdentifier($identifier);
                $bookSlug = $parts['bookSlug'];
                $langCode = $parts['languageCode'];
                $repoDir = sprintf('%s/%s/%s', $repoRoot, $bookSlug, $langCode);
                $repoParentDir = sprintf('%s/%s', $repoRoot, $bookSlug);
                if ($fs->exists($repoDir)) {
                    $directory = $repoDir;
                    if (is_writable($repoDir)) {
                        $fs->remove($repoDir);
                        $io->success(sprintf('Repo in directory "%s" was successfully removed.', $repoDir));
                    } else {
                        throw new RuntimeException(sprintf('Unable to write to the directory "%s".', $repoDir)); 
                    }
                } elseif ($fs->exists($repoParentDir)) {
                    $directory = $repoParentDir;
                    if (is_writable($repoParentDir)) {
                        $fs->remove($repoParentDir);
                        $io->success(sprintf('Repo in directory "%s" was successfully removed.', $repoParentDir));
                    } else {
                        throw new RuntimeException(sprintf('Unable to write to the directory "%s".', $repoParentDir));
                        
                    }
                } else {
                    throw new RuntimeException(sprintf('Unable to find a directory matching "%s" .', $directory));
                } 
            }
        }
        return Command::SUCCESS;
    }
}