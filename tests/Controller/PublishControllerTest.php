<?php

namespace App\Tests\Controller;

use App\Controller\PublishController;
use App\Model\Library;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\MailerAssertionsTrait;

class PublishControllerTest extends WebTestCase {
    
    use MailerAssertionsTrait;

    /**
     * Check that listen endpoint is working and sends mail
     */
    public function testListenAction() {
        $client = static::createClient();
        $client->enableProfiler();
        $client->catchExceptions(false);

        $hookBody = $this->getTestBookRequestBody();
        $headers = $this->getHeaders();
        $endpoint = '/admin/listen';

        $testBooksDir = $_ENV['BOOKS_DIR'];
        $testLibrary = new Library($testBooksDir);
        $container = static::getContainer();
        $container->library = $testLibrary;

        $publishController = $this->getMockBuilder(PublishController::class)
            ->disableOriginalConstructor()
            ->getMock();

        $client->request('POST', $endpoint, [], [], $headers, $hookBody);
        $statusCode = $client->getResponse()->getStatusCode();

        $this->assertEquals(Response::HTTP_OK, $statusCode);

        $this->assertEmailCount(1);

        $sentMessage = $this->getMailerMessage();

        $hookData = json_decode($hookBody, true);
        $sampleCommitHash = current($hookData['commits'])['id'];

        $this->assertEmailHtmlBodyContains($sentMessage, 'Publishing Successful');
        $this->assertEmailHtmlBodyContains($sentMessage, $sampleCommitHash);
    }

    /**
     * @return string
     */
    private function getTestBookRequestBody(): string {
        return file_get_contents(__DIR__ . '/../Files/webhook-gitlab-push-test-book.json');
    }

    /**
     * @return array
     */
    private function getHeaders(): array {
        $headers = [
            'HTTP_X-Gitlab-Event' => 'Push Hook', // prefix required for non-standard
            'Content-Type' => 'application/json'
        ];

        return $headers;
    }
}
