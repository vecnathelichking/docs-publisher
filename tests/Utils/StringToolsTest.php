<?php

namespace App\Tests\Utils;

use App\Utils\StringTools;

class StringToolsTest extends \PHPUnit\Framework\TestCase {
    /**
     * @param string $string
     *  The string to make safe for usage in URLs
     *
     * @param string $expected
     * The expected result
     *
     * @dataProvider urlProvider
     */
    public function testUrlSafe($string, $expected): void {
        $this->assertEquals($expected, StringTools::urlSafe($string));
    }

    /**
     * @return array
     */
    public function urlProvider(): array {
        return [
            [
                'PAGE-1',
                'page-1',
            ],
            [
                'this is a path',
                'this-is-a-path',
            ],
            [
                '***path 1',
                'path-1',
            ],
            [
                'another_path',
                'another-path',
            ],
        ];
    }

    /**
     * @param $rule
     *
     * @param $expected
     *
     * @dataProvider redirectRuleProvider
     */
    public function testParseRedirectRule($rule, $expected): void {
        $this->assertEquals($expected, StringTools::parseRedirectRule($rule));
    }

    /**
     * @return array
     */
    public function redirectRuleProvider(): array {
        return [
            [
                'foo/bar baz/bat',
                [
                    'from' => 'foo/bar',
                    'to' => 'baz/bat',
                    'type' => 'internal',
                ]
            ],
            [
                '/foo/bar/ /baz/bat/',
                [
                    'from' => 'foo/bar',
                    'to' => 'baz/bat',
                    'type' => 'internal',
                ]
            ],
            [
                'foo///bar baz///bat',
                [
                    'from' => 'foo///bar',
                    'to' => 'baz///bat',
                    'type' => 'internal',
                ]
            ],
            [
                "   /foo/bar   /baz/bat   \n",
                [
                    'from' => 'foo/bar',
                    'to' => 'baz/bat',
                    'type' => 'internal',
                ]
            ],
            [
                '#foo/bar baz/bat',
                null,
            ],
            [
                'foo/bar/baz/bat',
                null,
            ],
            [
                ' ',
                null,
            ],
            [
                'foo/bar baz/bat spam/eggs',
                [
                    'from' => 'foo/bar',
                    'to' => 'baz/bat',
                    'type' => 'internal',
                ]
            ],
        ];
    }
}
