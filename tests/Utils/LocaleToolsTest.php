<?php

namespace App\Tests\Utils;

use App\Utils\LocaleTools;

class LocaleToolsTest extends \PHPUnit\Framework\TestCase {
    /**
     * @param string $languageCode
     *
     * @param string $localeCode
     *
     * @param string $expected
     *
     * @dataProvider languageNameInLocaleProvider
     */
    public function testGetLanguageNameInLocale($languageCode, $localeCode, $expected): void {
        $this->assertEquals($expected, LocaleTools::getLanguageNameInLocale($languageCode, $localeCode));
    }

    /**
     * @return array
     */
    public function languageNameInLocaleProvider(): array {
        return [
            [
                'es',
                'en',
                'Spanish',
            ],
            [
                'es',
                'es',
                'español',
            ],
        ];
    }

    /**
     * @param string $languageCode
     *
     * @dataProvider codeProvider
     */
    public function testCodeIsValid($languageCode, $expected): void {
        $this->assertEquals($expected, LocaleTools::codeIsValid($languageCode));
    }

    public function codeProvider(): array {
        $validCodes = [
            'en',
            'es',
            'fr'
        ];
        $invalidCodes = [
            '',
            0,
            'English',
            'english',
            'qq',
            '00',
            'en-US'
        ];
        $validCodes = array_map(
            function ($i) {
                return [$i, true];
            },
            $validCodes
        );
        $invalidCodes = array_map(
            function ($i) {
                return [$i, false];
            },
            $invalidCodes
        );
        return array_merge($validCodes, $invalidCodes);
    }
}
